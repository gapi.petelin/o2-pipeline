PAPRICA_SPLITS=4
PAPRICA_PICK_DOMAIN_FILES = ["unique.count", \
                             "unique.fasta", \
                             "archaea.fasta", \
                             "archaeal16S.reads.txt", \
                             "bacteria.fasta", \
                             "bacterial16S.reads.txt", \
                             "eukarya.fasta", \
                             "eukaryote18S.reads.txt"]

rule all:
    input: expand("data/test.{type}", type=PAPRICA_PICK_DOMAIN_FILES)

rule split_fasta:
    input: "data/test.fasta"
    output: temp(expand("data/test.{number}.fasta", number=range(PAPRICA_SPLITS)))
    conda: "environments/split_fasta.yaml"
    shell: "pyfasta split -n {PAPRICA_SPLITS} {input}"
    
rule paprica_pick_domain:
    container: "docker://jsbowman/paprica"
    input: "data/test.{number}.fasta"
    output: temp(expand("data/test.{number}.{type}", allow_missing=True, type=PAPRICA_PICK_DOMAIN_FILES))
    shell:
        """
        input_str={input}
        paprica-pick_domain.py -in ${{input_str%.*}}
        """

rule merge_paprica_pick_domain:
    input: lambda wildcards: ["data/test.{0}.{1}".format(n, wildcards.type) for n in range(PAPRICA_SPLITS)]
    # Limit possible wildcard and prevent building infinite DAG
    wildcard_constraints: type="|".join(re.escape(word) for word in PAPRICA_PICK_DOMAIN_FILES)
    output: "data/test.{type}"
    shell: "cat {input} >> {output}"
