# o2-pipeline

## Snakemake setup

```bash
conda env create --prefix ./env -f environments/base.yaml
conda activate ./env/
```

## Snakemake run

### Local
```bash
snakemake -j1 --use-conda  --use-singularity
```

### Slurm
```bash
snakemake --profile .
```




